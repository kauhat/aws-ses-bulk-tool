package main

import (
	"fmt"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/aws/endpoints"
	"github.com/aws/aws-sdk-go-v2/aws/external"
	"github.com/aws/aws-sdk-go-v2/service/ses"
	"github.com/namsral/flag"
	"gopkg.in/cheggaaa/pb.v1"
)

type Config struct {
	configFile              string
	bounceTopic             string
	complaintTopic          string
	deliveryTopic           string
	forwardingEnabled       bool
	bounceHeadersEnabled    bool
	complaintHeadersEnabled bool
	deliveryHeadersEnabled  bool
}

func getConfig() Config {
	//
	var config Config

	// Get arguments
	flag.StringVar(&config.configFile, "config", "", "Get options from a config file")
	flag.StringVar(&config.bounceTopic, "bounce-topic", "", "The SNS topic used for bounces")
	flag.StringVar(&config.complaintTopic, "complaint-topic", "", "The SNS topic used for complaints")
	flag.StringVar(&config.deliveryTopic, "delivery-topic", "", "The SNS topic used for deliveries")
	flag.BoolVar(&config.forwardingEnabled, "forward-notifications", false, "Forward notifications as emails")
	flag.BoolVar(&config.bounceHeadersEnabled, "include-headers-on-bounce", false, "Include original email headers in the bounce SNS notifications")
	flag.BoolVar(&config.complaintHeadersEnabled, "include-headers-on-complaint", false, "Include original email headers in the complaint SNS notifications")
	flag.BoolVar(&config.deliveryHeadersEnabled, "include-headers-on-delivery", false, "Include original email headers in the delivery SNS notifications")
	flag.Parse()

	return config
}

func getAwsConnectionConfig() aws.Config {
	// Using the SDK's default configuration, loading additional config
	// and credentials values from the environment variables, shared
	// credentials, and shared configuration files
	cfg, err := external.LoadDefaultAWSConfig()
	if err != nil {
		panic("Unable to load SDK config, " + err.Error())
	}

	cfg.Region = endpoints.EuWest1RegionID

	return cfg
}

func getIdentities(client *ses.SES, identityType ses.IdentityType) []string {
	// Attempt to get a list of identities
	params := ses.ListIdentitiesInput{
		IdentityType: identityType, // EmailAddress or Domain
		MaxItems:     aws.Int64(1000),
		NextToken:    aws.String(""),
	}

	req := client.ListIdentitiesRequest(&params)
	result, err := req.Send()
	if err != nil {
		panic("Error: " + err.Error())
	}

	return result.Identities
}

type PendingNotificationAttributeChange struct {
	notificationType ses.NotificationType
	identity         string
	topic            string
}

func getTopicsToUpdate(client *ses.SES, identities []string, deliveryTopic string, bounceTopic string, complaintTopic string) (pending []PendingNotificationAttributeChange) {
	// Split identities into smaller chunks
	var chunks [][]string
	chunkSize := 100

	for i := 0; i < len(identities); i += chunkSize {
		chunkEnd := i + chunkSize
		if chunkEnd > len(identities) {
			chunkEnd = len(identities)
		}

		chunks = append(chunks, identities[i:chunkEnd])
	}

	// Get notification details
	for _, chunk := range chunks {
		params := ses.GetIdentityNotificationAttributesInput{
			Identities: chunk,
		}

		req := client.GetIdentityNotificationAttributesRequest(&params)
		result, err := req.Send()
		if err != nil {
			panic("Error: " + err.Error())
		}

		// Add non-matches to queue
		for identity, element := range result.NotificationAttributes {
			if aws.StringValue(element.BounceTopic) != bounceTopic {
				pending = append(pending, PendingNotificationAttributeChange{
					notificationType: ses.NotificationTypeBounce,
					identity:         identity,
					topic:            bounceTopic,
				})
			}

			if aws.StringValue(element.ComplaintTopic) != complaintTopic {
				pending = append(pending, PendingNotificationAttributeChange{
					notificationType: ses.NotificationTypeComplaint,
					identity:         identity,
					topic:            complaintTopic,
				})
			}

			if aws.StringValue(element.DeliveryTopic) != deliveryTopic {
				pending = append(pending, PendingNotificationAttributeChange{
					notificationType: ses.NotificationTypeDelivery,
					identity:         identity,
					topic:            deliveryTopic,
				})
			}
		}
	}

	return pending
}

func updateTopics(client *ses.SES, pending []PendingNotificationAttributeChange) {
	// Create a loading bar
	bar := pb.StartNew(len(pending))

	for _, change := range pending {
		bar.Increment()

		// Update the topic
		topicParams := ses.SetIdentityNotificationTopicInput{
			NotificationType: change.notificationType,
			Identity:         aws.String(change.identity),
		}

		if change.topic != "" {
			topicParams.SnsTopic = aws.String(change.topic)
		}

		req := client.SetIdentityNotificationTopicRequest(&topicParams)
		_, err := req.Send()
		if err != nil {
			panic("Error: " + err.Error())
		}
	}

	bar.Finish()
}

func main() {
	config := getConfig()

	// Create an SES client.
	svc := ses.New(getAwsConnectionConfig())

	// Get email and domain SES identities
	fmt.Printf("Getting identities...\n")
	var identities []string
	identities = append(identities, getIdentities(svc, ses.IdentityTypeEmailAddress)...)
	identities = append(identities, getIdentities(svc, ses.IdentityTypeDomain)...)

	fmt.Printf("Checking %d identities...\n", len(identities))
	pending := getTopicsToUpdate(svc, identities, config.deliveryTopic, config.bounceTopic, config.complaintTopic)

	fmt.Printf("Checked %d identities, %d changes to be made\n", len(identities), len(pending))
	if len(pending) > 0 {
		updateTopics(svc, pending)
	}

	fmt.Printf("Finished!\n")
}
