This tool is used to bulk update the SNS bounce and complaint topics for all SES identities. Please build the tool and use as below:

`./aws-ses-bulk-tool --bouncesTopic "full-topic-arn" --complaintsTopic "full-topic-arn"`