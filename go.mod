module gitlab.com/kauhat/aws-ses-bulk-tool

require (
	github.com/aws/aws-sdk-go-v2 v2.0.0-preview.4+incompatible
	github.com/go-ini/ini v1.42.0 // indirect
	github.com/jmespath/go-jmespath v0.0.0-20180206201540-c2b33e8439af // indirect
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/namsral/flag v1.7.4-pre
	gopkg.in/cheggaaa/pb.v1 v1.0.28
)
